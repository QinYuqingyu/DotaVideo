'use strict';

import React, {
    Component,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

import Video from 'react-native-video';
var Dimensions = require('Dimensions');
var { width, height } = Dimensions.get('window');
var Orientation = require('react-native-orientation');
import {BackButton,Loading} from '../common/Layout';
import {Icon} from '../common/Css';

module.exports = class VideoPlayer extends Component {
    constructor(props) {
        super(props);
        this.onLoad = this.onLoad.bind(this);
        this.onProgress = this.onProgress.bind(this);
    }

    state = {
        rate: 1,
        volume: 1,
        muted: false,
        resizeMode: 'contain',
        duration: 0.0,
        currentTime: 0.0,
        controls: true,
        paused: false,
        //skin: 'custom'
    };

    onLoad(data) {
        this.setState({duration: data.duration});
        console.log("==>onLoad", data, (new Date));
    }

    onProgress(data) {
        this.setState({currentTime: data.currentTime});
        //console.log("==>onProgress", data);


    }

    onLoadStart(data) {
        console.log("==>onLoadStart", data);


    }

    getCurrentTimePercentage() {
        if (this.state.currentTime > 0) {
            return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
        } else {
            return 0;
        }
    }

    setTime(percentage) {
        var time = this.state.duration * percentage;
        this.setState({
            currentTime: time
        });
    }


    renderSkinControl(skin) {
        const isSelected = this.state.skin == skin;
        const selectControls = skin == 'native' || skin == 'embed';
        return (
            <TouchableOpacity onPress={() => { this.setState({
          controls: selectControls,
          skin: skin
        }) }}>
                <Text style={[styles.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
                    {skin}
                </Text>
            </TouchableOpacity>
        );
    }

    renderRateControl(rate) {
        const isSelected = (this.state.rate == rate);

        return (
            <TouchableOpacity onPress={() => { this.setState({rate: rate}) }}>
                <Text style={[styles.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
                    {rate}x
                </Text>
            </TouchableOpacity>
        )
    }

    renderResizeModeControl(resizeMode) {
        const isSelected = (this.state.resizeMode == resizeMode);

        return (
            <TouchableOpacity onPress={() => { this.setState({resizeMode: resizeMode}) }}>
                <Text style={[styles.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
                    {resizeMode}
                </Text>
            </TouchableOpacity>
        )
    }

    renderVolumeControl(volume) {
        const isSelected = (this.state.volume == volume);

        return (
            <TouchableOpacity onPress={() => { this.setState({volume: volume}) }}>
                <Text style={[styles.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
                    {volume * 100}%
                </Text>
            </TouchableOpacity>
        )
    }


    renderNativeSkin() {
        var url = this.props.url;
        //var url = 'http://101.200.79.153/wxbVideos/?r=site/m3u8&url=http://v.youku.com/v_show/id_XMTUwMTczNDkzNg==.html';
        //console.log(this.state.controls);
        return (
            <View style={styles.container}>
                <View style={styles.fullScreen}>
                    <Video source={{uri: url}}
                           ref={'Video'}
                           style={styles.fullScreen}
                           rate={this.state.rate}
                           paused={this.state.paused}
                           volume={this.state.volume}
                           muted={this.state.muted}
                           resizeMode={this.state.resizeMode}
                           onLoadStart={this.onLoadStart}
                           onLoad={this.onLoad}
                           onSeek={this.seek}
                           onProgress={this.onProgress}
                           onEnd={() => {console.log('end')}}
                           repeat={true}
                           controls={this.state.controls}/>
                </View>
                <View style={[styles.controls]}>

                    <View style={[styles.controlsHead]}>
                        <View style={[styles.fRow]}>
                            <TouchableOpacity onPress={() => {this.goRouter()}}>
                                <Icon name='ios-arrow-left' size={22} color='#fff' />
                            </TouchableOpacity>
                            <Text style={styles.controlname}>黑暗吧</Text>
                        </View>
                        <View style={[styles.fRow, styles.controlGroup]}>
                            <View style={[styles.controlDpi]}>
                                <Text style={[styles.dpi]}>高清</Text>
                            </View>
                            <Icon name='ios-color-wand-outline' size={20} color='#fff' />
                            <Icon name='ios-star-half' size={20} color='#fff' />
                            <Icon name='ios-cloud-upload' size={20} color='#fff' />
                        </View>
                    </View>

                    <View style={[styles.controlsCenter]}>
                        <View style={[styles.light]}>
                        </View>
                        <View style={[styles.loadingIcon]}>
                            <Loading size={30} text="加载中..." color="#fff"/>
                            <Icon name='ios-play' size={20} color='#fff' />
                            <Icon name='ios-pause-outline' size={20} color='#fff' />
                        </View>
                    </View>

                    <View style={[styles.controlscrol]}>
                        <Text style={[styles.white]}>00:00</Text>
                        <View style={[styles.scroline]}>

                        </View>
                        <Text style={[styles.white]}>1:30:30</Text>
                    </View>
                </View>

            </View>
        );
    }

    componentWillMount() {
        this.setScreen(0);
        //this.refs.Video.seek(50)

    }

    componentDidMount() {
        //console.log(this.props);

        //console.log(this.props.navigator.getCurrentRoutes());

        //setTimeout(function(){
        //    this.setState({
        //        controls:false
        //    });
        //}.bind(this),500);

        //console.log('mount native render '+(new Date));

    }

    setScreen(type) {
        var type = type ? 1 : 0;
        if (type == 1) {
            Orientation.lockToPortrait(); //this will lock the view to Portrait

        }
        else {
            Orientation.lockToLandscape(); //this will lock the view to Landscape

        }
        //Orientation.unlockAllOrientations(); //this will unlock the view to all Orientations
    }

    goRouter(params) {
        this.refs['Video'].seek(50)
        return;
        this.setScreen(1);

        this.props.navigator.jumpBack();
        this.setState({
            paused: true,
        });
        //this.props.navigator.push({
        //    name: 'video',
        //    passProps: params
        //});
    }

    render() {

        return this.renderNativeSkin();
    }
}

const styles = StyleSheet.create({
    fRow: {
        flexDirection: 'row'
    },
    fColumn: {
        flexDirection: 'column'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    controls: {
        backgroundColor: 'rgba(0,0,0,0.9)',
        width: height,
        height: width,
        flex: 1,
        flexDirection: 'column',
    },

    controlsHead: {
        flexDirection: 'row',
        width: height,
        position: 'relative',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        paddingVertical: 10
    },

    controlsCenter: {
        flexDirection: 'row',
        width: height,
        height: width/2,
        position: 'relative',
        paddingHorizontal: 20,
        marginTop: 20
    },

    controlname: {
        color: '#fff',
        paddingVertical: 3,
        paddingHorizontal:10,
        width: width,
        overflow: 'hidden'
    },

    controlDpi: {
        borderRadius: 2,
        borderColor: '#ddd',
        borderWidth: 1,
        paddingHorizontal: 4,
        paddingVertical: 2,
        marginTop: 2
    },

    dpi: {
        color: '#ddd',
        fontSize: 10
    },

    controlGroup: {
        justifyContent: 'space-between',
        width: width/2,
    },

    light: {
        width:2,
        height: width/2-10,
        backgroundColor: '#fff'
    },

    loadingIcon: {
        width:height-22,
        height: width/2-10,
        justifyContent: 'center',
        alignItems: 'center'
    },

    controlscrol: {
        flexDirection: 'row',
        width: height,
        height:50,
        position: 'absolute',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        paddingVertical: 10,
        bottom:0,
        left:0,
    },

    white: {
        color: '#fff',
        fontSize: 11,
    },

    scroline: {
        width: height-140,
        borderColor: 'red',
        borderWidth:1
    }






});