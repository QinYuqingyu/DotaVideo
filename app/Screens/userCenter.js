/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
import {Header,Footer,Login} from '../common/Layout';
import {styles} from '../common/Css';
import UserCenter from '../component/UserCenter';

var {
    View
    } = React;



module.exports = React.createClass({

    getInitialState: function() {

        return {
            title:'用户中心'
        }
    },


    render: function() {

        return (
            <View>
                <Header title={this.state.title} {...this.props} />

                <View style={[styles.content]}>
                    <UserCenter navigator={this.props.navigator}  />
                </View>
                <Footer navigator={this.props.navigator} />

            </View>
        );
    },
});